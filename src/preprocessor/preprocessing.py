import numpy as np
from scipy import misc
from bs4 import BeautifulSoup
from sklearn.model_selection import train_test_split
from skimage.transform import rescale

import os

TARGET_ACTION_UNITS = {'25', '26', '45'}

def load_data(data_filepath):

    sessions = os.listdir(data_filepath + '/Sessions')

    action_unit_counts = {}
    image_arrays = []
    image_classes = []

    for session_id in sessions:
        session_filepath = '{}/Sessions/{}'.format(data_filepath, session_id)
        session_filenames = os.listdir(session_filepath)
        session_filenames.remove('session.xml')
        session_filenames.sort()

        image_filename = '{}/{}'.format(session_filepath, session_filenames[0])
        data_filename = '{}/{}'.format(session_filepath, session_filenames[1])

        data_soup = BeautifulSoup(open(data_filename, 'rb').read())

        action_units = [action_unit.attrs['number'] for action_unit in data_soup.find_all('actionunit')]

        for action_unit in action_units:
            if action_unit not in action_unit_counts:
                action_unit_counts[action_unit] = 1
            else:
                action_unit_counts[action_unit] += 1

        if not TARGET_ACTION_UNITS.isdisjoint(action_units):
            #image_arrays.append(misc.imread(image_filename, mode='L')[::4,::4])
            image_arrays.append(rescale(misc.imread(image_filename, mode='L'), 0.25) * 255)
            image_classes.append([int(member) for member in ['25' in action_units, '26' in action_units, '45' in action_units]])

    for action_unit, count in action_unit_counts.items():
        if count >= 50:
            print('{}:{}'.format(action_unit, count))

    features = np.array(image_arrays)
    features = np.expand_dims(features, axis=4)
    classes = np.array(image_classes)

    return train_test_split(features, classes, test_size=0.2, random_state=44)
