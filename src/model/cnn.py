import tensorflow as tf
from tqdm import tqdm
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_fscore_support
from sklearn.utils import shuffle
import pandas as pd

class Classifier():
    def __init__(self, sess, num_classes=3):
        self.sess = sess

        self.summary_dir = '../summary'
        self.checkpoint_dir = 'model/checkpoint'

        self.num_hidden = 12
        self.num_classes = num_classes

        self.training_steps = 2000
        self.batch_size = 12
        self.display_step = 40
        self.init_learning_rate = 0.000005
        self.dropout_keep = 0.4

        self.init_graph()

        self.dense, self.model = self.model()
        self.prediction = tf.round(self.model)

        self.loss_op = self.loss()
        self.optimizer = self.optimizer()
        self.train_op = self.optimizer.minimize(self.loss_op, global_step=self.global_step)
        self.correct_pred = self.correct_predictions()
        self.accuracy = self.accuracy(self.correct_pred)

    def init_graph(self):
        self.x = tf.placeholder(dtype=tf.float32, shape=[None, 400, 300, 1])
        self.y = tf.placeholder(dtype=tf.float32, shape=[None, self.num_classes])

        self.global_step = tf.Variable(0, trainable=False, name='global_step')
        self.learning_rate = tf.train.exponential_decay(self.init_learning_rate, self.global_step,
                                           80, 0.94, staircase=True)

        self.keep_prob = tf.placeholder(tf.float32)

    def update_from_config(self, name=' ', init_learning_rate=0.000005):
        self.name = name
        self.learning_rate = tf.train.exponential_decay(init_learning_rate, self.global_step,
                                           150, 0.94, staircase=True)

    def model(self):

        conv1 = tf.layers.conv2d(inputs=self.x, filters=32, kernel_size=[3, 3], padding='same', activation=tf.nn.relu)
        pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=3)
        conv2 = tf.layers.conv2d(inputs=pool1, filters=32, kernel_size=[2, 2], padding='same', activation=tf.nn.relu)
        pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)  # stride 1
        pool1_flat = tf.reshape(pool2, [-1, 105600])
        dense = tf.layers.dense(inputs=pool1_flat, units=256, activation=tf.nn.sigmoid)
        dropout = tf.layers.dropout(inputs=dense, rate=self.keep_prob)
        dense2 = tf.layers.dense(inputs=dropout, units=self.num_classes, activation=tf.nn.sigmoid)

        return dense, dense2

    def loss(self):
        return tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.model, labels=self.y)) + 0.0 * \
                                sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))

    def optimizer(self):
        return tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        #return tf.train.AdagradOptimizer(learning_rate=self.learning_rate)
        #return tf.train.GradientDescentOptimizer(learning_rate=self.learning_rate)

    def correct_predictions(self):
        return tf.equal(self.prediction, self.y)

    def accuracy(self, correct_predictions):
        return tf.reduce_mean(tf.cast(correct_predictions, tf.float32))

    def train(self, features, classes):
        print(self.name)
        #x_train, x_valid, y_train, y_valid = train_test_split(features, classes, test_size=0.2, random_state=33)
        x_train, x_valid, y_train, y_valid = train_test_split(features, classes, test_size=0.2, random_state=8)


        self.sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()

        best_valid_loss = [-1, self.num_classes * 1.0]
        data_over_time = {
            'steps': [],
            'validation_loss': [],
            'validation_accuracies': [],
            'learning_rate': [],
        }

        for step in tqdm(range(0, self.training_steps)):

            batch_x, batch_y = shuffle(x_train, y_train, n_samples=self.batch_size)
            self.sess.run([self.train_op], feed_dict={self.x: batch_x, self.y: batch_y})

            if step % self.display_step == 0:
                valid_loss, valid_acc, learn_rate, predictions = self.sess.run([self.loss_op, self.accuracy, self.learning_rate, self.model],
                                                    feed_dict={self.x: x_valid, self.y: y_valid, self.keep_prob: self.dropout_keep})
                data_over_time['steps'].append(step)
                data_over_time['validation_loss'].append(valid_loss)
                data_over_time['validation_accuracies'].append(valid_acc)
                data_over_time['learning_rate'].append(learn_rate)

                #print('Predictions: {}'.format(predictions))

                print("Step " + str(step) + ", Validation Loss: " + \
                      "{:.4f}".format(valid_loss) + ", Validation Accuracy: " + \
                      "{:.3f}".format(valid_acc) + ", Learning Rate: " + "{}".format(learn_rate))


                if valid_loss < best_valid_loss[1]:
                    saver.save(self.sess, self.checkpoint_dir + '/' + self.name + '/model.ckpt')
                    best_valid_loss = [step, valid_loss]

                if step - best_valid_loss[0] > 500:
                    break

        print('Best validation loss: {}'.format(best_valid_loss))

        validation_data = pd.DataFrame(data_over_time)
        validation_data.to_csv(self.checkpoint_dir + '/' + self.name + '/' + self.name + 'validation.csv')

    def load_module(self):
        saver = tf.train.Saver()
        saver.restore(self.sess, self.checkpoint_dir + '/' + self.name + '/model.ckpt')

    def get_feature_vector(self, features):
        dense_outputs = self.sess.run([self.dense], feed_dict={self.x: features, self.keep_prob: 1.0})
        return dense_outputs[0]

    def test(self, features, classes):
        self.load_module()
        loss, acc, predictions = self.sess.run([self.loss_op, self.accuracy, self.prediction], feed_dict={self.x: features,
                                                                self.y: classes, self.keep_prob: 1.0})

        precision, recall, fscore, _ = precision_recall_fscore_support(y_true=classes, y_pred=predictions)
        print('Test Loss: {}, Test Acc: {}, Test F1: {}'.format(loss, acc, fscore))