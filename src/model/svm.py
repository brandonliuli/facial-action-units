from sklearn.svm import SVC
from sklearn.metrics import precision_recall_fscore_support, accuracy_score
from sklearn.grid_search import GridSearchCV
import numpy as np

class SVM():
    def __init__(self, cnn, dimensions):
        self.cnn = cnn
        self.dimensions = dimensions

        self.models = []
        for dimension in range(0, dimensions):
            self.models.append(SVC(C=1e2, tol=1e-10, kernel='rbf'))

        self.parameter_space = [{'kernel': ['rbf'], 'gamma': [1e-1, 1e-2, 1e-3, 1e-4],
                             'C': [1, 10, 100, 1000]},
                            {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

    def train(self, features, classes):

        train_features = None
        for train_batch, test_batch in zip(np.array_split(features, 10), np.array_split(classes, 10)):
            if train_features is None:
                train_features = self.cnn.get_feature_vector(train_batch)
            else:
                train_features = np.concatenate([train_features, self.cnn.get_feature_vector(train_batch)])

        for dimension in range(0, self.dimensions):
            self.models[dimension].fit(train_features, classes[:, dimension])
            clf = GridSearchCV(self.models[dimension], self.parameter_space, cv=5, scoring='accuracy')
            clf.fit(train_features, classes[:, dimension])
            print('Best Params: {}'.format(clf.best_params_))
            self.models[dimension] = clf


    def predict(self, features):
        test_features = self.cnn.get_feature_vector(features)
        predictions = []
        for dimension in range(0, self.dimensions):
            predictions.append(self.models[dimension].predict(test_features))
        return np.array(predictions).T

    def score(self, features, classes):
        predictions = self.predict(features)
        accuracy = accuracy_score(y_pred=predictions, y_true=classes)
        precision, recall, fscore, _, = precision_recall_fscore_support(y_pred=predictions, y_true=classes, average=None)
        return {'accuracy': accuracy, 'precision': precision, 'recall': recall, 'fscore': fscore}



