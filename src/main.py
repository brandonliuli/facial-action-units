from src.preprocessor.preprocessing import load_data
from src.model.cnn import Classifier
import tensorflow as tf
import numpy as np
from src.model.svm import SVM

DATA_FILEPATH = '../data'
TRAINING_MODE = 0
TESTING_MODE = 1

MODE = TESTING_MODE

def main():
    x_train, x_test, y_train, y_test = load_data(DATA_FILEPATH)

    configs = [
               {'name': 'Trial_A', 'init_learning_rate': 1e6},
    ]

    for run_config in configs:
        process_config(run_config, x_train, x_test, y_train, y_test)


def process_config(run_config, x_train, x_test, y_train, y_test):
    with tf.device('/gpu:0'):
        tf_config = tf.ConfigProto(allow_soft_placement=True)
        tf_config.gpu_options.allow_growth = True
        sess = tf.Session(config=tf_config)
        with tf.Graph().as_default(), sess:
            clf = Classifier(sess=sess)
            clf.update_from_config(**run_config)
            if MODE == TRAINING_MODE:
                clf.train(x_train, y_train)

            if MODE == TESTING_MODE:
                clf.load_module()
                svm = SVM(cnn=clf, dimensions=3)
                svm.train(x_train, y_train)

                print('{}'.format(svm.score(x_test, y_test)))



if __name__ == '__main__':
    main()